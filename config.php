<?php  /// Moodle Configuration File 

unset($CFG);

$CFG = new stdClass();
$CFG->dbtype    = 'mysql';
$CFG->dbhost    = 'localhost';
$CFG->dbname    = 'moodle2';
$CFG->dbuser    = 'progin';
$CFG->dbpass    = 'progin';
$CFG->dbpersist =  false;
$CFG->prefix    = 'mdl_';

$CFG->wwwroot   = 'http://localhost/oddyseus_frontend-moodle';
$CFG->dirroot   = 'C:\\xampp\\htdocs\\oddyseus_frontend-moodle';
$CFG->dataroot  = 'C:\\xampp\\htdocs\\moodledata';
$CFG->admin     = 'admin';

//$CFG = new stdClass();
//$CFG->dbtype    = 'mysql';
//$CFG->dbhost    = 'localhost';
//$CFG->dbname    = 'moodle2';
//$CFG->dbuser    = 'root';
//$CFG->dbpass    = 'tokitoki';
//$CFG->dbpersist =  false;
//$CFG->prefix    = 'mdl_';
//
//$CFG->wwwroot   = 'http://167.205.32.27/moodle-1.9';
//$CFG->dirroot   = '/var/www/html/moodle-1.9';
//$CFG->dataroot  = '/var/www/moodledata';
//$CFG->admin     = 'admin';

$CFG->directorypermissions = 00777;  // try 02777 on a server in Safe Mode

$CFG->passwordsaltmain = '9,VC<D4jIYhX*VpST:B</_VKX';

require_once("$CFG->dirroot/lib/setup.php");
// MAKE SURE WHEN YOU EDIT THIS FILE THAT THERE ARE NO SPACES, BLANK LINES,
// RETURNS, OR ANYTHING ELSE AFTER THE TWO CHARACTERS ON THE NEXT LINE.
?>
