<?php
/**
 * Unit tests for this question type.
 *
 * @copyright &copy; 2012 Karol Danutama
 * @author karoldanutama@gmail.com
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package LX
 *//** */
    
require_once(dirname(__FILE__) . '/../../../../config.php');

global $CFG;
require_once($CFG->libdir . '/simpletestlib.php');
require_once($CFG->dirroot . '/question/type/sourcecode/questiontype.php');

class sourcecode_qtype_test extends UnitTestCase {
    var $qtype;
    
    function setUp() {
        $this->qtype = new sourcecode_qtype();
    }
    
    function tearDown() {
        $this->qtype = null;    
    }

    function test_name() {
        $this->assertEqual($this->qtype->name(), 'sourcecode');
    }
    
    // TODO write unit tests for the other methods of the question type class.
}

?>
