<?php // $Id: qtype_sourcecode.php,v 1.2 2006/08/25 21:40:44 tjhunt Exp $
/**
 * The language strings for the Source Code question type.
 *    
 * @copyright &copy; 2012 Karol Danutama
 * @author karoldanutama@gmail.com
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package LX
 *//** */

$string['addingsourcecode'] = 'Adding source code question';
$string['editingsourcecode'] = 'Editing Source Code';
$string['sourcecode'] = 'Source Code';
$string['evaluatorfiles'] = 'Evaluator files (zip)';
$string['compiletype'] = 'Compile type';
$string['gradingtype'] = 'Grading type';
$string['languages'] = 'Languages';


$string['Single File'] = 'Single File';
$string['Multi File'] = 'Multi File';
$string['Simple'] = 'Simple';
$string['Batch'] = 'Batch';

$string['C'] = 'C';
$string['C++'] = 'C++';
$string['Java'] = 'Java';
$string['LISP'] = 'LISP';
$string['Pascal'] = 'Pascal';
$string['PHP'] = 'PHP';
$string['Python'] = 'Python';

?>
