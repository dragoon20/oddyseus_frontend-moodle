<?php

/**
 * The editing form code for this question type.
 *
 * @copyright &copy; 2012 Karol Danutama
 * @author karoldanutama@gmail.com
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package LX
 *//** */
require_once($CFG->dirroot . '/question/type/edit_question_form.php');

/**
 * Source code editing form definition.
 * 
 * See http://docs.moodle.org/en/Development:lib/formslib.php for information
 * about the Moodle forms library, which is based on the HTML Quickform PEAR library.
 */
class question_edit_sourcecode_form extends question_edit_form {

    function definition_inner(&$mform) {
        // TODO, add any form fields you need.
        // $mform->addElement( ... );
        $mform->addElement('file', 'evaluatorfiles', get_string("evaluatorfiles", "qtype_sourcecode"));
        $typeoptions = array('singlesource' => get_string('Single File', 'qtype_sourcecode'), 'archive' => get_string('Multi File', 'qtype_sourcecode'));
        $mform->addElement('select', 'compiletype', get_string('compiletype', 'qtype_sourcecode'), $typeoptions);
        // $typeoptions = array( 'simple' => get_string('Simple', 'qtype_sourcecode'), 'batch' => get_string('Batch', 'qtype_sourcecode'));
        $typeoptions = array('batch' => get_string('Batch', 'qtype_sourcecode'));
        $mform->addElement('select', 'gradingtype', get_string("gradingtype", "qtype_sourcecode"), $typeoptions);

        $langs = array();
        $langs[] = & $mform->createElement('checkbox', 'C', '', get_string('C', 'qtype_sourcecode'));
        $langs[] = & $mform->createElement('checkbox', 'C++', '', get_string('C++', 'qtype_sourcecode'));
        $langs[] = & $mform->createElement('checkbox', 'Java', '', get_string('Java', 'qtype_sourcecode'));
        $langs[] = & $mform->createElement('checkbox', 'LISP', '', get_string('LISP', 'qtype_sourcecode'));
        $langs[] = & $mform->createElement('checkbox', 'Pascal', '', get_string('Pascal', 'qtype_sourcecode'));
        $langs[] = & $mform->createElement('checkbox', 'PHP', '', get_string('PHP', 'qtype_sourcecode'));
        $langs[] = & $mform->createElement('checkbox', 'Python', '', get_string('Python', 'qtype_sourcecode'));
        $mform->addGroup($langs, 'langs', get_string('languages', 'qtype_sourcecode'), null, true);
    }

    function set_data($question) {
        $detail = get_record("question_sourcecode", "question", $question->id);
        if ($detail) {
            $question->compiletype = $detail->compiletype;
            $question->gradingtype = $detail->gradingtype;
//            echo 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX<br />';
//            echo json_encode($detail, JSON_PRETTY_PRINT);
//            echo json_encode(explode(";", $detail->languages), JSON_PRETTY_PRINT);
            
            foreach (explode(";", $detail->languages) as $value) {
                $question->langs[$value] = 1;
            }
            
//            echo json_encode($question, JSON_PRETTY_PRINT);
//            die();
        }

        parent::set_data($question);
    }

    function validation($data) {
        $errors = array();

        // TODO, do extra validation on the data that came back from the form. E.g.
        // if (/* Some test on $data['customfield']*/) {
        //     $errors['customfield'] = get_string( ... );
        // }

        if ($errors) {
            return $errors;
        } else {
            return true;
        }
    }

    function qtype() {
        return 'sourcecode';
    }

}

?>
